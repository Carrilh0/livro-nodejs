const http = require('http');
const fs = require('fs');

const server = http.createServer((request,response)=>{
    //_dirname retorna o diretorio raiz da aplicação
    if (request.url === '/'){
    fs.readFile(`${__dirname}/index.html`,(erro,html)=>{
        response.writeHead(200,{'Content-Type':'text/html'});
        response.write(html);
        response.end();
    });
    }else if(request.url === '/artigos'){
    fs.readFile(`${__dirname}/artigos.html`,(erro,artigos)=>{
        response.writeHead(200,{'Content-Type':'text/html'});
        response.write(artigos);
        response.end();
    });
    }else if(request.url === '/contato'){
    fs.readFile(`${__dirname}/contato.html`,(erro,contato)=>{
        response.writeHead(200,{'Content-Type':'text/html'});
        response.write(contato);
        response.end();
    });
    }else{
    fs.readFile(`${__dirname}/erro.html`,(erro,error)=>{
        response.writeHead(200,{'Content-Type':'text/html'});
        response.write(error);
        response.end();
    });
    }
});

server.listen(3000, () =>{
    console.log('Executando site pessoal');
});